<?php
namespace AppBoss\GuzzleHttp\Exception;

class TransferException extends \RuntimeException implements GuzzleException {}
