<?php
namespace AppBoss\GuzzleHttp\Exception;

class TooManyRedirectsException extends RequestException {}
